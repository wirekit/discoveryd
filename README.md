Discoveryd
-----------
Discoveryd provides a transcient service advertisement hub. This means that services self advertise to a single or a cluster of available discovery servers where the only guarantees are that all services while accessable will always advertise to others interested.

With Discoveryd, services meta-data are not stored on the servers and once a server goes down all service advertised get disconnected. Discovery embodies the principle of possible failures and provides self discoverable cluster communication where multiple clusters can locate themselves through a central server at initial startup, whilst the others can move on to negotiate and communicate without the central server. This allows the central server not to be a heavily relied upon component apart from the servers initial start if they do not have proper information about themselves.

Discoveryd also kills any services who may have exceeded it's live-liness expectations or failed to respond within adequate expectations, ensuring the stability of servers and clients having opportunity to reestablish their status.


## Install

```go
go get github.com/wirekit/discoveryd
```

## Running

## Command Line Arguments