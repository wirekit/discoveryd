ci: coverage tests benchmarks

coverage:
	go test -cover ./disco/...

tests:
	go test -v ./disco/...

benchmarks:
	go test -v -run=xXX -bench=. ./disco/...

dist-windows-amd64:
    CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build -v -a -tags netgo -installsuffix netgo -ldflags '-s -w -X github.com/wirekit/discoveryd.gitCommit=`git rev-parse --short HEAD`' -o ./dist/linux/windows-amd64/discoveryd.exe

dist-linux-amd64:
    CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -v -a -tags netgo -installsuffix netgo -ldflags '-s -w -X github.com/wirekit/discoveryd.gitCommit=`git rev-parse --short HEAD`' -o ./dist/linux/amd64/discoveryd

dist-linux-arm6:
    CGO_ENABLED=0 GOOS=linux GOARCH=arm GOARM=6 go build -v -a -tags netgo -installsuffix netgo -ldflags '-s -w -X github.com/wirekit/discoveryd.gitCommit=`git rev-parse --short HEAD`' -o ./dist/linux/arm6/discoveryd

dist-linux-arm7:
    CGO_ENABLED=0 GOOS=linux GOARCH=arm GOARM=7 go build -v -a -tags netgo -installsuffix netgo -ldflags '-s -w -X github.com/wirekit/discoveryd.gitCommit=`git rev-parse --short HEAD`' -o ./dist/linux/arm7/discoveryd

dist-linux-arm64:
    CGO_ENABLED=0 GOOS=linux GOARCH=arm64 go build -v -a -tags netgo -installsuffix netgo -ldflags '-s -w -X github.com/wirekit/discoveryd.gitCommit=`git rev-parse --short HEAD`' -o ./dist/linux/arm64/discoveryd

