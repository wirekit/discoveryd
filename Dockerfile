FROM scratch
MAINTAINER EWETUMO ALEXANDER <trinoxf@gmail.com>

RUN apk --no-cache add ca-certificates

ADD ./dist/discoveryd /usr/bin/discoveryd

ENV PORT 4880
EXPOSE 4880

ENTRYPOINT ["discoveryd"]
CMD ["--help"]