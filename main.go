package main

import (
	"crypto/tls"
	"strings"

	"github.com/gokit/history"
	"github.com/gokit/history/handlers/std"
	"github.com/influx6/faux/flags"
	"github.com/wirekit/discoveryd/disco"
)

func main() {
	history.SetDefaultHandlers(std.Std)

	flags.Run("discoveryd", flags.Command{
		Name:         "run",
		AllowDefault: true,
		Desc:         "starts the discoveryd server",
		Flags: []flags.Flag{
			&flags.StringFlag{
				Name:    "addr",
				Default: "tcp://0.0.0.0:4880",
				Desc:    "set the uri address to be used (supports tcp 'tcp' or websocket 'ws' schemes)",
			},
			&flags.StringFlag{
				Name: "cert",
				Desc: "set path to tls certificate to be used for TLS/SSL",
			},
			&flags.StringFlag{
				Name: "key",
				Desc: "set path to tls certificate key to be used for TLS/SSL",
			},
			&flags.BoolFlag{
				Name: "tls",
				Desc: "set bool value use TLS/SSL for listening",
			},
			&flags.StringFlag{
				Name: "clusters",
				Desc: "comma separated discoveryd servers on other hosts e.g 'tcp://10.10.0.1:9010,tcp://40.10.100.1:2050'",
			},
		},
		Action: func(ctx flags.Context) error {
			useTLS := ctx.GetBool("tls")

			var tlsConfig *tls.Config

			if useTLS {
				certFile := ctx.GetString("key")
				keyFile := ctx.GetString("cert")

				logctx := history.WithFields(history.Attrs{
					"certFile": certFile,
					"keyFile":  keyFile,
				})

				var config tls.Config
				if certFile == "" && keyFile == "" {
					config.InsecureSkipVerify = true
				} else {
					certificate, err := tls.LoadX509KeyPair(certFile, keyFile)
					if err != nil {
						logctx.Error(err, "failed to load cert file and key file for tls")
						return err
					}

					config.Certificates = append(config.Certificates, certificate)
				}

				tlsConfig = &config
			}

			var server disco.Service
			server.TLS = tlsConfig
			server.Addr = ctx.GetString("addr")
			server.Clusters = strings.Split(ctx.GetString("clusters"), ",")

			if err := server.Start(ctx); err != nil {
				return err
			}

			server.Wait()
			return nil
		},
	})
}
